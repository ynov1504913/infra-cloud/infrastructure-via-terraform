resource "azurerm_lb" "lb_prod" {
    name = "lb-${var.company_name}-prod"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    sku = "Standard"

    frontend_ip_configuration {
        name = "pip_lb-bcw-prod"
        public_ip_address_id = azurerm_public_ip.pip_lb_prod.id
    }
}

resource "azurerm_public_ip" "pip_lb_prod" {
    name = "pip-lb-${var.company_name}-prod"
    resource_group_name = azurerm_resource_group.rg.name
    location = var.location
    allocation_method = "Static"
    sku = "Standard"
}

resource "azurerm_lb_backend_address_pool" "backend_pool_lb_prod" {
    name = "backend-pool-lb-${var.company_name}-prod"
    loadbalancer_id = azurerm_lb.lb_prod.id
}

resource "azurerm_lb_backend_address_pool_address" "backend_pool_address_lb_prod_1" {
    name = "backend-pool-address-lb-${var.company_name}-prod-1"
    backend_address_pool_id = azurerm_lb_backend_address_pool.backend_pool_lb_prod.id
    ip_address = azurerm_linux_virtual_machine.vm1_prod.private_ip_address
    virtual_network_id = azurerm_virtual_network.vn.id
}

resource "azurerm_lb_backend_address_pool_address" "backend_pool_address_lb_prod_2" {
    name = "backend-pool-address-lb-${var.company_name}-prod-2"
    backend_address_pool_id = azurerm_lb_backend_address_pool.backend_pool_lb_prod.id
    ip_address = azurerm_linux_virtual_machine.vm2_prod.private_ip_address
    virtual_network_id = azurerm_virtual_network.vn.id
}