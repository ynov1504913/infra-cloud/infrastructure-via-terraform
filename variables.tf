variable "company_name" {
  type = string
  description = "value of the company name"
}

variable "adress_space" {
  type = list(string)
  description = "Plage d'adressage que vont prendre les différentes ressources présentes dans mon réseau virtuel"
}

variable "location" {
  type = string
  description = "value of the location"
  default = "North Europe"
}

variable "admin_username" {
  type = string
  description = "value of the admin username"
}
  
variable "admin_password" {
  type = string
  description = "value of the admin password"
}