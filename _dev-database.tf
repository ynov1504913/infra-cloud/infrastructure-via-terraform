resource "random_password" "password_db_server_dev" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_mssql_server" "db_server_dev" {
  name                         = "db-server-${var.company_name}-dev"
  resource_group_name          = azurerm_resource_group.rg.name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = "admin_db_server_dev"
  administrator_login_password = random_password.password_db_server_dev.result
}

resource "azurerm_mssql_database" "db_dev" {
  name           = "db-${var.company_name}-dev"
  server_id      = azurerm_mssql_server.db_server_dev.id
  collation      = "SQL_Latin1_General_CP1_CI_AS"
  license_type   = "LicenseIncluded"
  max_size_gb    = 1
  read_scale     = false
  sku_name       = "S0"
  zone_redundant = false
}