resource "random_password" "password_vm2_prod" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_public_ip" "pip_vm2_prod" {
  name                = "pip-vm2-${var.company_name}-prod"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "ni_vm2_prod" {
  name                = "ni-vm2-${var.company_name}-prod"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "ipconfig-ni-vm2-prod"
    subnet_id                     = azurerm_subnet.sub_prod.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "vm2_prod" {
  name                = "vm2-${var.company_name}-prod"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  size                = "Standard_F2"
  disable_password_authentication = false
  admin_username      = "admin_vm2_prod"
  admin_password      = random_password.password_vm2_prod.result
  network_interface_ids = [
    azurerm_network_interface.ni_vm2_prod.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}