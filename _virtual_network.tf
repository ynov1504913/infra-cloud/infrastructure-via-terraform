resource "azurerm_virtual_network" "vn" {
  name                = "vn-${var.company_name}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  address_space       = var.adress_space
}

resource "azurerm_subnet" "sub_adm" {
  name                 = "sub-${var.company_name}-adm"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vn.name
  address_prefixes     = [azurerm_virtual_network.vn.address_space[0]]
}

resource "azurerm_subnet" "sub_dev" {
  name                 = "sub-${var.company_name}-dev"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vn.name
  address_prefixes     = [azurerm_virtual_network.vn.address_space[1]]
}

resource "azurerm_subnet" "sub_prod" {
  name                 = "sub-${var.company_name}-prod"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vn.name
  address_prefixes     = [azurerm_virtual_network.vn.address_space[2]]
}

resource "azurerm_public_ip" "pip" {
  name                = "pip-vn-${var.company_name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

resource "azurerm_nat_gateway" "ng" {
  name                    = "ng-vn-${var.company_name}"
  location                = var.location
  resource_group_name     = azurerm_resource_group.rg.name
  sku_name                = "Standard"
}