resource "azurerm_resource_group" "rg" {
  name     = "rg-${var.company_name}"
  location = var.location
}