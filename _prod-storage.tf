resource "azurerm_storage_account" "sa_prod" {
  name                     = "sa${var.company_name}prod543"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  network_rules {
    default_action             = "Allow"
    virtual_network_subnet_ids = [azurerm_subnet.sub_prod.id]
  }
}

resource "azurerm_storage_account_network_rules" "sa_nr_prod" {
  storage_account_id = azurerm_storage_account.sa_prod.id

  default_action             = "Allow"
  virtual_network_subnet_ids = [azurerm_subnet.sub_prod.id]
}

resource "azurerm_storage_container" "sc_prod" {
  name                  = "sc-${var.company_name}-prod"
  storage_account_name  = azurerm_storage_account.sa_prod.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "blob_prod" {
  name                  = "blob-${var.company_name}-prod"
  storage_account_name   = azurerm_storage_account.sa_prod.name
  storage_container_name = azurerm_storage_container.sc_prod.name
  type                   = "Block"
}