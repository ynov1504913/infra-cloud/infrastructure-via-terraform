resource "random_password" "password_db_server_prod" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_mssql_server" "db_server_prod" {
  name                         = "db-server-${var.company_name}-prod"
  resource_group_name          = azurerm_resource_group.rg.name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = "admin_db_server_prod"
  administrator_login_password = random_password.password_db_server_prod.result
}

resource "azurerm_mssql_database" "db_prod" {
  name           = "db-${var.company_name}-prod"
  server_id      = azurerm_mssql_server.db_server_prod.id
  collation      = "SQL_Latin1_General_CP1_CI_AS"
  license_type   = "LicenseIncluded"
  max_size_gb    = 1
  read_scale     = false
  sku_name       = "S0"
  zone_redundant = false
}