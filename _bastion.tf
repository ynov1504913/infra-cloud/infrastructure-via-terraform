resource "random_password" "password_bastion" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_public_ip" "pip_bastion" {
  name                = "pip-bastion-${var.company_name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "ni_bastion" {
  name                = "ni-bastion-${var.company_name}"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "ipconfig-ni-bastion"
    subnet_id                     = azurerm_subnet.sub_adm.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pip_bastion.id
  }
}

resource "azurerm_linux_virtual_machine" "vm_bastion" {
  name                = "vm-bastion-${var.company_name}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  size                = "Standard_F2"
  disable_password_authentication = false
  admin_username      = "admin_bastion"
  admin_password      = random_password.password_bastion.result
  network_interface_ids = [
    azurerm_network_interface.ni_bastion.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}