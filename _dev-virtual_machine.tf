resource "random_password" "password_vm_dev" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_network_interface" "ni_dev" {
  name                = "ni-vm-${var.company_name}-dev"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "ipconfig-ni-vm-dev"
    subnet_id                     = azurerm_subnet.sub_dev.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "vm_dev" {
  name                = "vm-${var.company_name}-dev"
  resource_group_name = azurerm_resource_group.rg.name
  location            = var.location
  size                = "Standard_F2"
  disable_password_authentication = false
  admin_username      = "admin_vm_dev"
  admin_password      = random_password.password_vm_dev.result
  network_interface_ids = [
    azurerm_network_interface.ni_dev.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}