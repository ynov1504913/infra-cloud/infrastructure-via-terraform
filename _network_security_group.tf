resource "azurerm_network_security_group" "nsg_adm"{
    name                = "nsg-sn-bastion-${var.company_name}"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name

    security_rule {
        name                       = "AllowSshInbound"
        priority                   = 100
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "DenyHttpInbound"
        priority                   = 110
        direction                  = "Inbound"
        access                     = "Deny"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "DenyHttpsInbound"
        priority                   = 120
        direction                  = "Inbound"
        access                     = "Deny"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_security_group" "nsg_prod"{
    name                = "nsg-sn-vm-${var.company_name}"
    location            = var.location
    resource_group_name = azurerm_resource_group.rg.name

    security_rule {
        name                       = "AllowHttpInbound"
        priority                   = 100
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "AllowHttpsInbound"
        priority                   = 110
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "AllowSshInboundFromBastion"
        priority                   = 120
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = azurerm_linux_virtual_machine.vm_bastion.private_ip_address
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_security_group" "nsg_dev"{
    name                = "nsg-sn-vm-${var.company_name}-dev"
    location            = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name

    security_rule {
        name                       = "AllowHttpInbound"
        priority                   = 100
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "192.168.0.0/16"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "AllowHttpsInbound"
        priority                   = 110
        direction                  = "Inbound"
        access                     = "Deny"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "192.168.0.0/16"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "AllowSshInboundFromBastion"
        priority                   = 120
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = azurerm_linux_virtual_machine.vm_bastion.private_ip_address
        destination_address_prefix = "*"
    }
}

resource "azurerm_subnet_network_security_group_association" "nsg_asso_adm" {
  subnet_id                 = azurerm_subnet.sub_adm.id
  network_security_group_id = azurerm_network_security_group.nsg_adm.id
}

resource "azurerm_subnet_network_security_group_association" "nsg_asso_prod" {
  subnet_id                 = azurerm_subnet.sub_prod.id
  network_security_group_id = azurerm_network_security_group.nsg_prod.id
}

resource "azurerm_subnet_network_security_group_association" "nsg_asso_dev" {
  subnet_id                 = azurerm_subnet.sub_dev.id
  network_security_group_id = azurerm_network_security_group.nsg_dev.id
}