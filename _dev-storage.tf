resource "azurerm_storage_account" "sa_dev" {
  name                     = "sa${var.company_name}dev543"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  network_rules {
    default_action             = "Allow"
    virtual_network_subnet_ids = [azurerm_subnet.sub_dev.id]
  }
}

resource "azurerm_storage_account_network_rules" "sa_nr_dev" {
  storage_account_id = azurerm_storage_account.sa_dev.id

  default_action             = "Allow"
  virtual_network_subnet_ids = [azurerm_subnet.sub_dev.id]
}

resource "azurerm_storage_container" "sc_dev" {
  name                  = "sc-${var.company_name}-dev"
  storage_account_name  = azurerm_storage_account.sa_dev.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "blob_dev" {
  name                  = "blob-${var.company_name}-dev"
  storage_account_name   = azurerm_storage_account.sa_dev.name
  storage_container_name = azurerm_storage_container.sc_dev.name
  type                   = "Block"
}